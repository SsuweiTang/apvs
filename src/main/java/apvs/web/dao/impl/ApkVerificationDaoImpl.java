package apvs.web.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.JdbcTemplate;

import apvs.web.dao.ApkVerificationDao;
import apvs.web.dto.APKFingerPrintInfo;
import apvs.web.dto.APKManifest;
import apvs.web.rowmapper.APKFingerPrintRowMapper;
import apvs.web.rowmapper.APKManifestRowMapper;

/**
 * @author Carl Adler (C.A.)
 * */
public class ApkVerificationDaoImpl implements ApkVerificationDao, InitializingBean{

	private static final String SQL_FOR_QUERY_ALL_APK_INFO = "select * from apk_info;";
	private static final String SQL_FOR_QUERY_SPECIFIC_APK_INFO = "select * from apk_info where apk_file_name = ?;";
	private static final String SQL_FOR_QUERY_SPECIFIC_VERIFICATION_RESULT = "select verification_result from apk_verification_history where query_identifier = ?;";
	private static final String SQL_FOR_INSERT_VERIFICATION_RESULT = "insert into apk_verification_history (query_identifier, verification_result) values (?, ?);";
	private static final String SQL_FOR_QUERY_SPECIFIC_APK_FP_INFO = "select * from apk_fp_info where id = ?;";
	private JdbcTemplate jdbcTemplate;
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		if (dataSource == null) {
			throw new BeanCreationException("Must set dataSource on ApkVerifyDao");
			}
	}

	@Override
	public List<APKManifest> getAllApkInfo() {
		return jdbcTemplate.query(SQL_FOR_QUERY_ALL_APK_INFO, new APKManifestRowMapper());
	}

	@Override
	public APKManifest getSpecificApkInfoWithFileName(String apkFileName) {
		return jdbcTemplate.queryForObject(SQL_FOR_QUERY_SPECIFIC_APK_INFO, new Object[] {apkFileName}, new APKManifestRowMapper());
	}
	
	@Override
	public String queryVerificationResultByQueryIdentifier(String queryIdentifier) {
		return jdbcTemplate.queryForObject(SQL_FOR_QUERY_SPECIFIC_VERIFICATION_RESULT, String.class, new Object[] {queryIdentifier});
	}

	@Override
	public boolean saveVerificationResult(String queryIdentifier, String result) {
		return jdbcTemplate.update(SQL_FOR_INSERT_VERIFICATION_RESULT, new Object[] {queryIdentifier, result}) > 0;
	}

	@Override
	public APKFingerPrintInfo getSpecificApkFingerPrintInfoWithid(long id) {
		return jdbcTemplate.queryForObject(SQL_FOR_QUERY_SPECIFIC_APK_FP_INFO, new Object[] {id}, new APKFingerPrintRowMapper());
		
	}

}
