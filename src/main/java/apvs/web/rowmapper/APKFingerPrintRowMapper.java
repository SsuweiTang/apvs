package apvs.web.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import apvs.web.dto.APKFingerPrintInfo;
import apvs.web.dto.APKManifest;

public class APKFingerPrintRowMapper implements RowMapper<APKFingerPrintInfo>{

	@Override
	public APKFingerPrintInfo mapRow(ResultSet rs, int rowNum)throws SQLException {
		
		APKFingerPrintInfo apkFingerPrintInfo=new APKFingerPrintInfo();
		apkFingerPrintInfo.setId(rs.getLong("id"));
		apkFingerPrintInfo.setFingerPrintForEntireAPKFile(rs.getString("fp_entire_apk"));
		apkFingerPrintInfo.setFingerPrintForAndroidManifestXml(rs.getString("fp_android_mnft"));
		apkFingerPrintInfo.setFingerPrintForResourcesArsc(rs.getString("fp_res_arsc"));
		apkFingerPrintInfo.setFingerPrintForClassesDex(rs.getString("fp_class_dex"));
		apkFingerPrintInfo.setFingerPrintForRes(rs.getString("fp_res"));
		apkFingerPrintInfo.setFingerPrintForResXml(rs.getString("fp_res_xml"));
		apkFingerPrintInfo.setFingerPrintForResImg(rs.getString("fp_res_img"));
		return apkFingerPrintInfo;
	}

}
