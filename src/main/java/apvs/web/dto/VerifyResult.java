package apvs.web.dto;

import java.io.Serializable;

import lombok.Data;

public @Data class VerifyResult implements Serializable {
	private static final long serialVersionUID = 1L;
	
	String upload;
	int verifyResult;
	String verifyMessage;
	private String verifyFingerPrintForResXml;
	private String verifyFingerPrintForResImg;
	private String verifyFingerPrintForClassesDex;
	private String verifyFingerPrintForAndroidManifestXml;
	private String verifyFingerPrintForResourcesArsc;
	private String verifyFingerPrintForRes;
	private String verifyFingerPrintForEntireAPKFile;
	private String verifyError;
	

}
