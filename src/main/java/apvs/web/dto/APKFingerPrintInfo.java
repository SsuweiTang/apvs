package apvs.web.dto;

import java.io.Serializable;

import lombok.Data;

public @Data class APKFingerPrintInfo implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	private Long id;
	private String fingerPrintForResXml;
	private String fingerPrintForResImg;
	private String fingerPrintForClassesDex;
	private String fingerPrintForAndroidManifestXml;
	private String fingerPrintForResourcesArsc;
	private String fingerPrintForRes;
	private String fingerPrintForEntireAPKFile;
	private String uploadFileInfo;

}
