package apvs.web.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import apvs.web.dto.APKFingerPrintInfo;
import apvs.web.dto.APKManifest;
import apvs.web.dto.VerifyResult;

/**
 * @author Carl Adler (C.A.)
 * */
public interface ApvsService {
	
	public List<APKManifest> obtainAllAplSamplesInDB();
	
	public String verifyApkIntegrity(APKManifest apkManifest);
	
	public VerifyResult verifyApkFingerPrintIntegrity(String apkFileName,MultipartFile file) throws Exception;
	
	public VerifyResult verifySha1AndApkFingerPrintIntegrity(String apkFileName,MultipartFile file,String androidManifestXml,String resourcesArsc,String classesDex,String mnftEntrySize,String versionCode) throws Exception;

	
	
}
