package apvs.web.service;

import apvs.web.dto.APKFingerPrintInfo;

/**
 * 
 * @author Ssu-Wei,Tang
 *
 */
public interface APKFingerPrintService {
	
	public APKFingerPrintInfo generateAPKFingerPrintFromAPKFile(String apkFile);

}
