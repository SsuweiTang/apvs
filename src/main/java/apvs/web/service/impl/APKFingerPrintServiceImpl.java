package apvs.web.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import apvs.web.dto.APKFingerPrintInfo;
import apvs.web.dto.APKSubFileFingerPrint;
import apvs.web.service.APKFingerPrintService;
import apvs.web.util.APVSConstantDescriptor;
import apvs.web.util.fingerprint.APKFingerPrintGenerator;
import apvs.web.util.fingerprint.APKFingerPrintHandler;
import apvs.web.util.fingerprint.ObjectTransformer;
import apvs.web.util.fingerprint.SHA1Generator;



/**
 * 
 * @author Ssu-Wei Tang
 *
 */
@Service
public class APKFingerPrintServiceImpl implements APKFingerPrintService {
	
	
	@Override
	public APKFingerPrintInfo generateAPKFingerPrintFromAPKFile(String apkFile) {
		JarFile apkSourceFile;
		APKFingerPrintInfo apkFingerPrint = null;
		try {
			if (new File(apkFile).getName().endsWith(".apk")) {
				apkSourceFile = new JarFile(apkFile);
				System.out.printf("---APK 特徵值產生模組開始---\n");
				apkFingerPrint = getAPKFingerPrint(apkSourceFile);
				System.out.printf("---APK 特徵值產生模組完成---\n");
				return apkFingerPrint;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return apkFingerPrint;
	}
	
	private static APKFingerPrintInfo getAPKFingerPrint(JarFile apkSourceFile) throws IOException {
		TreeMap<String, JarEntry> entryMap = APKFingerPrintHandler.getAPKFileMap(apkSourceFile);
		List<APKSubFileFingerPrint> fingerPrintListForResXml = new ArrayList<APKSubFileFingerPrint>();
		List<APKSubFileFingerPrint> fingerPrintListForResImg = new ArrayList<APKSubFileFingerPrint>();
		List<APKSubFileFingerPrint> fingerPrintListForRes = new ArrayList<APKSubFileFingerPrint>();
		APKFingerPrintInfo apkFingerPrint = new APKFingerPrintInfo();

		for (JarEntry entry : entryMap.values()) {
			String name = entry.getName();
			if (!isMETAINFResources(entry)) {
				if (!isPrimaryResources(name)) {
					if(isSecondaryResource(name))
					{
						if (name.endsWith(APVSConstantDescriptor.RES_CATEGORY_XML)) {
							fingerPrintListForResXml.add(SHA1Generator.generateSha1ForFile(apkSourceFile.getInputStream(entry), name));
						} else if (isImageResources(name)) {		
							fingerPrintListForResImg.add(SHA1Generator.generateSha1ForFile(apkSourceFile.getInputStream(entry), name));
						}
					}
				} else if (isPrimaryResources(name)) {
					if (name.equals(APVSConstantDescriptor.CLASSES_DEX)) {
						apkFingerPrint.setFingerPrintForClassesDex(APKFingerPrintGenerator.generateFingerPrintForPrimaryResource(apkSourceFile, entry, name));
					} else if (name.equals(APVSConstantDescriptor.ANDROID_MANIFEST_XML)) {
						apkFingerPrint.setFingerPrintForAndroidManifestXml(APKFingerPrintGenerator.generateFingerPrintForPrimaryResource(apkSourceFile, entry, name));
					} else if (name.equals(APVSConstantDescriptor.RESOURCES_ARSC)) {
						apkFingerPrint.setFingerPrintForResourcesArsc(APKFingerPrintGenerator.generateFingerPrintForPrimaryResource(apkSourceFile, entry, name));
						
					}
				}
			}
		}
		if(fingerPrintListForResImg.size()>0)
			apkFingerPrint.setFingerPrintForResImg(APKFingerPrintGenerator.generateFingerPrintForSubCategoryOfSecondaryResource(fingerPrintListForResImg));
		else
			apkFingerPrint.setFingerPrintForResImg("0000000000000000000000000000000000000000");
		
		
		if(fingerPrintListForResXml.size()>0)
				apkFingerPrint.setFingerPrintForResXml(APKFingerPrintGenerator.generateFingerPrintForSubCategoryOfSecondaryResource(fingerPrintListForResXml));
		else
			apkFingerPrint.setFingerPrintForResXml("0000000000000000000000000000000000000000");
		
		
		
		fingerPrintListForRes.add(ObjectTransformer.transformSubCategoryOfResToListAPKSubFileFingerPrint(APVSConstantDescriptor.RES_CATEGORY_XML, apkFingerPrint.getFingerPrintForResXml()));
		fingerPrintListForRes.add(ObjectTransformer.transformSubCategoryOfResToListAPKSubFileFingerPrint(APVSConstantDescriptor.RES_CATEGORY_PNG, apkFingerPrint.getFingerPrintForResImg()));
		
		apkFingerPrint.setFingerPrintForRes(APKFingerPrintGenerator.generateFingerPrintForSecondaryResource(fingerPrintListForRes));
		apkFingerPrint.setFingerPrintForEntireAPKFile(APKFingerPrintGenerator.generateFingerPrintForEntireAPKFile(apkFingerPrint));
		
		APKFingerPrintHandler.deleteDirectoryForCutFile(new File(apkSourceFile.getName().substring(0, apkSourceFile.getName().indexOf(".apk"))));
		
		apkSourceFile.close();		
		return apkFingerPrint;
	}

	private static boolean isMETAINFResources(JarEntry entry) {
		boolean result = true;
		String entryName = entry.getName();
		if(!entry.isDirectory() && !entryName.equals(JarFile.MANIFEST_NAME) && !entryName.equals(APVSConstantDescriptor.CERT_SF) && !entryName.equals(APVSConstantDescriptor.CERT_RSA)) {
			result = false;
		
		}
		else
		{
//			System.out.println("entry"+entry);
		}
		
		return result;
	}
	
	private static boolean isPrimaryResources(String name) {
		boolean result = false;
		if(name.equals(APVSConstantDescriptor.RESOURCES_ARSC)
				||name.equals(APVSConstantDescriptor.ANDROID_MANIFEST_XML)
				|| name.equals(APVSConstantDescriptor.CLASSES_DEX)) {
			 result = true;
		}
		return result;
	}
	
	private static boolean isImageResources(String name) {
		boolean result = false;
		if(name.endsWith(APVSConstantDescriptor.RES_CATEGORY_PNG) || name.endsWith(APVSConstantDescriptor.RES_CATEGORY_JPG)) {
			result = true;
		}
		return result;
	}
	
	
	private static boolean isSecondaryResource(String name) {
		boolean result = false;
		if(name.startsWith("res")||name.equalsIgnoreCase("R")) {
			result = true;
		}
		return result;
	}
}
	
	
	
	