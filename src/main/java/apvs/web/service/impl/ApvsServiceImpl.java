package apvs.web.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import apvs.web.dao.ApkVerificationDao;
import apvs.web.dto.APKFingerPrintInfo;
import apvs.web.dto.APKManifest;
import apvs.web.dto.VerifyResult;
import apvs.web.service.APKFingerPrintService;
import apvs.web.service.ApvsService;
import apvs.web.util.APVSConstantDescriptor;
import apvs.web.util.ApkFingerPrintComparator;
import apvs.web.util.ApkMetaInfoComparator;

/**
 * @author Carl Adler (C.A.)
 * */
@Service
public class ApvsServiceImpl implements ApvsService{
	
	private static final Logger logger = Logger.getLogger(ApvsServiceImpl.class);

	private ApkVerificationDao apkVerificationDao;
	
	private APKFingerPrintService  apkFingerPrintService;
	public String myApkfileName;
	
	public void setApkFingerPrintService(APKFingerPrintService apkFingerPrintService) {
		this.apkFingerPrintService = apkFingerPrintService;
	}

	public void setApkVerificationDao(ApkVerificationDao apkVerificationDao) {
		this.apkVerificationDao = apkVerificationDao;
	}
	
	@Override
	public List<APKManifest> obtainAllAplSamplesInDB() {
		return apkVerificationDao.getAllApkInfo();
	}

	@Override
	public String verifyApkIntegrity(APKManifest unverified) {
//		System.out.println(unverified.getApkFileName());
//		System.out.println(unverified.getMnftEntrySize());
//		System.out.println(unverified.getShaForAndroidManifestXml());
//		System.out.println(unverified.getShaForClassesDex());
//		System.out.println(unverified.getShaForResourcesArsc());
//		System.out.println(unverified.getVersionCode());
		
		
		String verifyResult = APVSConstantDescriptor.LIGHT_DANGEROUS;
		String verifyMessage = APVSConstantDescriptor.MESSAGE_FOR_DANGEROUS;
		APKManifest verified = null;
		try {				
			 verified = apkVerificationDao.getSpecificApkInfoWithFileName(unverified.getApkFileName());
		} catch (Exception e) {
			if(e.getMessage().contains("Incorrect result size: expected 1, actual 0")) {
				logger.debug("No matched apk in db.");
			}
		}
		
		if(verified == null) {
			verifyResult = APVSConstantDescriptor.LIGHT_UNKNOWN;
			verifyMessage = APVSConstantDescriptor.MESSAGE_FOR_UNKNOWN;
		} else {
			int resultCode = ApkMetaInfoComparator.compareApkManifestInfo(unverified, verified);
			
			if(resultCode == APVSConstantDescriptor.RESULT_CODE_FOR_PASSED){
				verifyResult = APVSConstantDescriptor.LIGHT_PASSED;
				verifyMessage = APVSConstantDescriptor.MESSAGE_FOR_PASSED;
			} else if(resultCode == APVSConstantDescriptor.RESULT_CODE_FOR_VERSION_MISMATCH) {
				verifyResult = APVSConstantDescriptor.LIGHT_VERSION_MISMATCH;
				verifyMessage = APVSConstantDescriptor.MESSAGE_FOR_VERSION_MISMATCH;
			}		
		} 
		logger.debug("Apk integrity verification result: " + verifyMessage);
		logger.debug("Saving verification result to db...");
		//apkVerificationDao.saveVerificationResult(unverified.getQueryIdentifier(), verifyResult);		
		logger.debug("Verification result saved completely.");
//		System.out.println("verifyResult: " + verifyResult);
		return verifyResult;
		
	}

	
	

	public String  uploadAPKFile(String apkFileName,MultipartFile file) {
		String UploadInfo;
		String uploadedFileLocation = APVSConstantDescriptor.UPLOADED_FILE_LOCATION+ apkFileName;
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(uploadedFileLocation)));
                stream.write(bytes);
                stream.close();
                UploadInfo="success";
            } catch (Exception e) {
            	 UploadInfo="failed";
            	 logger.debug(e.getMessage());
            }
        } else {
        	 UploadInfo="failed";
        	 logger.debug("Apk file is empty.");
        }
		return UploadInfo;
	}

	

	@Override
	public VerifyResult verifyApkFingerPrintIntegrity(String apkFileName, MultipartFile file) throws Exception {
			
		APKFingerPrintInfo	verifiedFingerPrint=null;
		APKFingerPrintInfo unverifiedFingerPrint = null;
		VerifyResult apkFingerPrintVerifyResult=new VerifyResult();
		
		String uploadApkFileName=now()+apkFileName;
		System.out.println("-----App上傳模組開始-----");
		
		
		if(uploadAPKFile(uploadApkFileName,file).equals("success"))
		{
			System.out.println("-----App上傳成功-----");
			apkFingerPrintVerifyResult.setUpload("success");
			if(new File(APVSConstantDescriptor.UPLOADED_FILE_LOCATION+uploadApkFileName).exists())
				unverifiedFingerPrint=apkFingerPrintService.generateAPKFingerPrintFromAPKFile(APVSConstantDescriptor.UPLOADED_FILE_LOCATION+uploadApkFileName);
		
			try {			
				long Id=apkVerificationDao.getSpecificApkInfoWithFileName(apkFileName).getId();
//				 System.out.println("id  "+Id);		
					 verifiedFingerPrint = apkVerificationDao.getSpecificApkFingerPrintInfoWithid(Id);	
			} catch (Exception e) {
				if(e.getMessage().contains("Incorrect result size: expected 1, actual 0")) {
					logger.debug("No matched apk in db.");
				}
			}		
			 if(verifiedFingerPrint==null)
			 {
				 apkFingerPrintVerifyResult.setVerifyResult(APVSConstantDescriptor.RESULT_CODE_FOR_UNKNOWN);
				 apkFingerPrintVerifyResult.setVerifyMessage(APVSConstantDescriptor.MESSAGE_FOR_UNKNOWN);
			 }
			 else
			 {
				 apkFingerPrintVerifyResult=ApkFingerPrintComparator.compareFingerPrintInfo(unverifiedFingerPrint, verifiedFingerPrint);	
			 }
			 	try {
					deleteDownloadFile(uploadApkFileName);
				} catch (Exception e) {
					throw new Exception("Delete file error.", e);
				}
		}else
		{
			System.out.println("-----App上傳失敗-----");
		}
		
		
		return apkFingerPrintVerifyResult;
	}
	
	
	public static String now() {
		 Calendar tmpCal = Calendar.getInstance();
		 SimpleDateFormat tmpSDF = new SimpleDateFormat("HHmmssSSSSS_");
		 return tmpSDF.format(tmpCal.getTime());
}
	
	private boolean deleteDownloadFile(String apkFileName) throws Exception{		
		File deletedFile = new File(APVSConstantDescriptor.UPLOADED_FILE_LOCATION + apkFileName);
		System.out.println("-----App檔案刪除-----");
		return deletedFile.delete();
	}

	@Override
	public VerifyResult verifySha1AndApkFingerPrintIntegrity (
			String apkFileName, MultipartFile file, String androidManifestXml,String resourcesArsc, String classesDex, String mnftEntrySize,String versionCode) throws Exception{
		
		VerifyResult apkSha1AndFingerPrintVerifyResult=new VerifyResult();
		VerifyResult apkFingerPrintVerifyResult=verifyApkFingerPrintIntegrity(apkFileName,file);
		APKManifest unverified=new  APKManifest();
		unverified.setApkFileName(apkFileName);
		unverified.setMnftEntrySize(Integer.parseInt(mnftEntrySize));
		unverified.setShaForAndroidManifestXml(androidManifestXml);
		unverified.setShaForResourcesArsc(resourcesArsc);
		unverified.setShaForClassesDex(classesDex);
		unverified.setVersionCode(versionCode);
		String resultSha=verifyApkIntegrity(unverified);
//		System.out.println("resultSha=="+resultSha+apkFingerPrintVerifyResult.getVerifyResult());
		
		if(resultSha.equals("green")&&apkFingerPrintVerifyResult.getVerifyResult()==1)
		{
			apkFingerPrintVerifyResult.setVerifyMessage(APVSConstantDescriptor.MESSAGE_FOR_PASSED);
			apkFingerPrintVerifyResult.setVerifyResult(APVSConstantDescriptor.RESULT_CODE_FOR_PASSED);
		}
		else if(resultSha.equals("yellow")||apkFingerPrintVerifyResult.getVerifyResult()==3)
		{
			apkFingerPrintVerifyResult.setVerifyResult(APVSConstantDescriptor.RESULT_CODE_FOR_UNKNOWN);
			apkFingerPrintVerifyResult.setVerifyMessage(APVSConstantDescriptor.MESSAGE_FOR_UNKNOWN);
		}
		else if(resultSha.equals("green")&&apkFingerPrintVerifyResult.getVerifyResult()==4)
		{
			apkFingerPrintVerifyResult.setVerifyResult(APVSConstantDescriptor.RESULT_CODE_FOR_DANGEROUS);
			apkFingerPrintVerifyResult.setVerifyMessage(APVSConstantDescriptor.MESSAGE_FOR_DANGEROUS);
			apkFingerPrintVerifyResult.setVerifyError(apkFingerPrintVerifyResult.getVerifyError());
		}
		else if(resultSha.equals("red")&&apkFingerPrintVerifyResult.getVerifyResult()==4)
		{
			apkFingerPrintVerifyResult.setVerifyResult(APVSConstantDescriptor.RESULT_CODE_FOR_DANGEROUS);
			apkFingerPrintVerifyResult.setVerifyMessage(APVSConstantDescriptor.MESSAGE_FOR_DANGEROUS);
			apkFingerPrintVerifyResult.setVerifyError(apkFingerPrintVerifyResult.getVerifyError());
		}
		
		else if(resultSha.equals("blue"))
		{
			apkFingerPrintVerifyResult.setVerifyResult(APVSConstantDescriptor.RESULT_CODE_FOR_VERSION_MISMATCH);
			apkFingerPrintVerifyResult.setVerifyMessage(APVSConstantDescriptor.MESSAGE_FOR_VERSION_MISMATCH);
			apkFingerPrintVerifyResult.setVerifyError(apkFingerPrintVerifyResult.getVerifyError());
		}
		
		System.out.println("App資料及特徵值比對模組結果  result code=="+apkFingerPrintVerifyResult.getVerifyResult());
		return apkFingerPrintVerifyResult;
	}
	
	

}
