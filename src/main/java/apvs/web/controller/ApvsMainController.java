package apvs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import apvs.web.dto.VerifyResult;
import apvs.web.service.ApvsService;
import apvs.web.util.ApkInfoTransformer;

/**
 * @author Carl Adler (C.A.)
 * */
@Controller
public class ApvsMainController {
	
	@Autowired
	ApvsService apvsService;
	@RequestMapping(value = "/verifyapkfile", method = RequestMethod.POST)
	public @ResponseBody String verifyApkFile(@RequestBody LinkedMultiValueMap<String, String> unverifiedApkInfo){
		return apvsService.verifyApkIntegrity(ApkInfoTransformer.transformReqMapIntoAplInfoObj(unverifiedApkInfo));
	}
	
	
	@RequestMapping(value = "/verifyapkfile", method = RequestMethod.GET)
	public @ResponseBody String verifyApkFile(){
		return "You can verify file by posting to this same URL.";
	}
	
	
	
	 @RequestMapping(value="/verifyapkfingerprint", method=RequestMethod.GET)
	    public @ResponseBody String handleFileUpload() {
	        return "You can upload a file by posting to this same URL.";
	    }
	 
	 
	 
	@RequestMapping(value = "/verifyapkfingerprint", method = RequestMethod.POST)
	public @ResponseBody VerifyResult handleFileUpload(
			@RequestParam("filename") String filename,
			@RequestParam("file") MultipartFile file) {
		
		VerifyResult result = null;
		try {
			result = apvsService.verifyApkFingerPrintIntegrity(filename, file);
		} catch (Exception e) {
			System.out.println("Verification failed: " + e.getMessage());
		}
		
		return result;
	}
	
	
	@RequestMapping(value = "/verifysha1andapkfingerprint", method = RequestMethod.POST)
	public @ResponseBody VerifyResult verifySha1andApkFingerprint(
			@RequestParam("filename") String filename,
			@RequestParam("androidManifestXml") String androidManifestXml,
			@RequestParam("resourcesArsc") String resourcesArsc,
			@RequestParam("classesDex") String classesDex,
			@RequestParam("mnftEntrySize") String mnftEntrySize,
			@RequestParam("versionCode") String versionCode,
			@RequestParam("file") MultipartFile file) {
		VerifyResult result = null;
		
		try {
			result = apvsService.verifySha1AndApkFingerPrintIntegrity(filename, file,androidManifestXml,resourcesArsc,classesDex,mnftEntrySize,versionCode);
		} catch (Exception e) {
			System.out.println("Verification for SHA1 and fingerprint failed: " + e.getMessage());
		}
		return result;
	}

	
	
	
}
