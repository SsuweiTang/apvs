package apvs.web.util;

import org.apache.log4j.Logger;

import apvs.web.dto.APKFingerPrintInfo;
import apvs.web.dto.APKManifest;
import apvs.web.dto.VerifyResult;

public class ApkFingerPrintComparator {
private static Logger logger = Logger.getLogger(ApkFingerPrintComparator.class);
	
	public static VerifyResult compareFingerPrintInfo(APKFingerPrintInfo unverified, APKFingerPrintInfo verified) {
		
		logger.debug("---APK FingerPrint integrity validation process start---");
		
		System.out.printf("---APK 特徵值比對模組開始---\n");
		
		int result = APVSConstantDescriptor.RESULT_CODE_FOR_DANGEROUS;
		
		VerifyResult apkFingerPrintVerifyResult=new VerifyResult();
		
		apkFingerPrintVerifyResult.setUpload("success");
		apkFingerPrintVerifyResult.setVerifyResult(APVSConstantDescriptor.RESULT_CODE_FOR_DANGEROUS);
		apkFingerPrintVerifyResult.setVerifyMessage(APVSConstantDescriptor.MESSAGE_FOR_DANGEROUS);
		apkFingerPrintVerifyResult.setVerifyFingerPrintForEntireAPKFile("1");
		apkFingerPrintVerifyResult.setVerifyFingerPrintForAndroidManifestXml("1");
		apkFingerPrintVerifyResult.setVerifyFingerPrintForClassesDex("1");
		apkFingerPrintVerifyResult.setVerifyFingerPrintForResourcesArsc("1");
		apkFingerPrintVerifyResult.setVerifyFingerPrintForRes("1");
		apkFingerPrintVerifyResult.setVerifyFingerPrintForResXml("1");
		apkFingerPrintVerifyResult.setVerifyFingerPrintForResImg("1");
		
		boolean assertForFingerPrintForEntireAPKFile=(verified.getFingerPrintForEntireAPKFile().equals(unverified.getFingerPrintForEntireAPKFile()));
		boolean assertForFingerPrintForAndroidManifestXml=(verified.getFingerPrintForAndroidManifestXml().equals(unverified.getFingerPrintForAndroidManifestXml()));
		boolean  assertForFingerPrintForClassesDex=(verified.getFingerPrintForClassesDex().equals(unverified.getFingerPrintForClassesDex()));
		boolean  assertForFingerPrintForResourcesArsc=(verified.getFingerPrintForResourcesArsc().equals(unverified.getFingerPrintForResourcesArsc()));
		boolean  assertForFingerPrintForRes=(verified.getFingerPrintForRes().equals(unverified.getFingerPrintForRes()));
		boolean  assertForFingerPrintForResImg=(verified.getFingerPrintForResImg().equals(unverified.getFingerPrintForResImg()));
		boolean  assertForFingerPrintForResXml=(verified.getFingerPrintForResXml().equals(unverified.getFingerPrintForResXml()));

		String error = "";
		if (assertForFingerPrintForEntireAPKFile&& assertForFingerPrintForAndroidManifestXml&& assertForFingerPrintForClassesDex&& assertForFingerPrintForResourcesArsc
				&& assertForFingerPrintForRes && assertForFingerPrintForResImg&& assertForFingerPrintForResXml) {
				
				apkFingerPrintVerifyResult.setVerifyMessage(APVSConstantDescriptor.MESSAGE_FOR_PASSED);
				apkFingerPrintVerifyResult.setVerifyResult(APVSConstantDescriptor.RESULT_CODE_FOR_PASSED);
				
		} else {
			if (!assertForFingerPrintForEntireAPKFile) {
//				error+=APVSConstantDescriptor.FINGERPRINT_FOR_ENTIREAPKFILE+",";
				apkFingerPrintVerifyResult.setVerifyFingerPrintForEntireAPKFile("0");
			}
			if (!assertForFingerPrintForAndroidManifestXml) {
				error=APVSConstantDescriptor.MESSAGE_FOR_DANGEROUS_ANDROID_MANIFEST_XML+",";
				apkFingerPrintVerifyResult.setVerifyFingerPrintForAndroidManifestXml("0");
			}
			if (!assertForFingerPrintForClassesDex) {
				error+=APVSConstantDescriptor.MESSAGE_FOR_DANGEROUS_CLASSES_DEX+",";
				apkFingerPrintVerifyResult.setVerifyFingerPrintForClassesDex("0");
			}
			if (!assertForFingerPrintForResourcesArsc) {
				error+=APVSConstantDescriptor.MESSAGE_FOR_DANGEROUS_RESOURCES_ARSCL+",";
				apkFingerPrintVerifyResult.setVerifyFingerPrintForResourcesArsc("0");
			}
			if (!assertForFingerPrintForRes) {
//				error+=APVSConstantDescriptor.FINGERPRINT_FOR_RES+",";
				apkFingerPrintVerifyResult.setVerifyFingerPrintForRes("0");
			}
			if (!assertForFingerPrintForResImg) {
				error+=APVSConstantDescriptor.MESSAGE_FOR_DANGEROUS_RES_IMG+",";
				apkFingerPrintVerifyResult.setVerifyFingerPrintForResImg("0");
			}
			if (!assertForFingerPrintForResXml) {
				error+=APVSConstantDescriptor.MESSAGE_FOR_DANGEROUS_RES_XML+",";
				apkFingerPrintVerifyResult.setVerifyFingerPrintForResXml("0");
			}
			apkFingerPrintVerifyResult.setVerifyError(error);
		}
		showComparedResult(unverified, verified);
		logger.debug("	Compare result: " + apkFingerPrintVerifyResult.getVerifyResult());
		logger.debug("---APK integrity validation process finished---");
		
		System.out.printf("	比對結果:  %s\n" , apkFingerPrintVerifyResult.getVerifyResult());
		System.out.printf("---App 特徵值比對模組完成---\n\n" );
//		System.out.println("error  "+error);
		return apkFingerPrintVerifyResult;
		
	}
	
	private static void showComparedResult(APKFingerPrintInfo unverified, APKFingerPrintInfo verified) {
		logger.debug("	Compared MANIFEST.MF info:  " + unverified);
		showMnftInfo(unverified);
		logger.debug("	Original MANIFEST.MF info: " + verified);
		showMnftInfo(verified);
		System.out.println("***********比對App特徵值 資訊**********\n");
		showMnftInfoWithConsole(unverified);
		System.out.println("**********未驗證App特徵值 資訊*********\n");
		showMnftInfoWithConsole(verified);
	}
	
	private static void showMnftInfo(APKFingerPrintInfo unverified) {
		logger.debug(String.format("	FingerPrint for %s: %s","id", unverified.getId()));
		logger.debug(String.format("	FingerPrint for %s: %s", APVSConstantDescriptor.FINGERPRINT_FOR_ENTIREAPKFILE, unverified.getFingerPrintForEntireAPKFile()));
		logger.debug(String.format("	FingerPrint for %s: %s", APVSConstantDescriptor.FINGERPRINT_ANDROID_MANIFEST_XML, unverified.getFingerPrintForAndroidManifestXml()));
		logger.debug(String.format("	FingerPrint for %s: %s", APVSConstantDescriptor.FINGERPRINT_RESOURCES_ARSC, unverified.getFingerPrintForResourcesArsc()));
		logger.debug(String.format("	FingerPrint for %s: %s", APVSConstantDescriptor.FINGERPRINT_CLASSES_DEX, unverified.getFingerPrintForClassesDex()));
		logger.debug(String.format("	FingerPrint for %s: %s", APVSConstantDescriptor.FINGERPRINT_FOR_RES, unverified.getFingerPrintForRes()));
		logger.debug(String.format("	FingerPrint for %s: %s", APVSConstantDescriptor.FINGERPRINT_FOR_RES_IMG, unverified.getFingerPrintForResImg()));
		logger.debug(String.format("	FingerPrint for %s: %s", APVSConstantDescriptor.FINGERPRINT_FOR_RES_XML, unverified.getFingerPrintForResXml()));
	}
	
	public static void showMnftInfoWithConsole(APKFingerPrintInfo unverified) {
		System.out.printf(String.format("	FingerPrint for %s: %s\n","id", unverified.getId()));
		System.out.printf(String.format("	FingerPrint for %s: %s\n", APVSConstantDescriptor.FINGERPRINT_FOR_ENTIREAPKFILE, unverified.getFingerPrintForEntireAPKFile()));
		System.out.printf(String.format("	FingerPrint for %s: %s\n", APVSConstantDescriptor.FINGERPRINT_ANDROID_MANIFEST_XML, unverified.getFingerPrintForAndroidManifestXml()));
		System.out.printf(String.format("	FingerPrint for %s: %s\n", APVSConstantDescriptor.FINGERPRINT_RESOURCES_ARSC, unverified.getFingerPrintForResourcesArsc()));
		System.out.printf(String.format("	FingerPrint for %s: %s\n", APVSConstantDescriptor.FINGERPRINT_CLASSES_DEX, unverified.getFingerPrintForClassesDex()));
		System.out.printf(String.format("	FingerPrint for %s: %s\n", APVSConstantDescriptor.FINGERPRINT_FOR_RES, unverified.getFingerPrintForRes()));
		System.out.printf(String.format("	FingerPrint for %s: %s\n", APVSConstantDescriptor.FINGERPRINT_FOR_RES_IMG, unverified.getFingerPrintForResImg()));
		System.out.printf(String.format("	FingerPrint for %s: %s\n", APVSConstantDescriptor.FINGERPRINT_FOR_RES_XML, unverified.getFingerPrintForResXml()));

	}
}
