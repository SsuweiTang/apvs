package apvs.web.util;


import org.apache.log4j.Logger;

import apvs.web.dto.APKManifest;

/**
 * @author Carl Adler (C.A.)
 * */
public class ApkMetaInfoComparator {
	
	private static Logger logger = Logger.getLogger(ApkMetaInfoComparator.class);
	
	public static int compareApkManifestInfo(APKManifest unverified, APKManifest verified) {
		
		logger.debug("---APK integrity validation process start---");
		
		System.out.printf("---APK 資料比對模組開始---\n");
		
		int result = APVSConstantDescriptor.RESULT_CODE_FOR_DANGEROUS;
		boolean assertForMnftSize = (verified.getMnftEntrySize() == unverified.getMnftEntrySize());
		boolean assertForAndroidManifestXml = (verified.getShaForAndroidManifestXml().equals(unverified.getShaForAndroidManifestXml()));
		boolean assertForResourcesArsc = (verified.getShaForResourcesArsc().equals(unverified.getShaForResourcesArsc()));
		boolean assertForClassesDex = (verified.getShaForClassesDex().equals(unverified.getShaForClassesDex()));
		boolean assertForVersionCode = (verified.getVersionCode().equals(unverified.getVersionCode()));
		
		if(assertForVersionCode) {
			if(assertForMnftSize) {
				if(assertForAndroidManifestXml && assertForResourcesArsc && assertForClassesDex)
					result = APVSConstantDescriptor.RESULT_CODE_FOR_PASSED;
			}
		} else {
			result = APVSConstantDescriptor.RESULT_CODE_FOR_VERSION_MISMATCH;
		}
		
		showComparedResult(unverified, verified);
		System.out.printf("	比對結果:  %s\n" , result);
		logger.debug("	Compare result: " + result);
		System.out.printf("---App 資料比對模組完成---\n\n" );
//		logger.debug("---APK integrity validation process finished---");
		return result;
		
	}
	
	private static void showComparedResult(APKManifest unverified, APKManifest verified) {
		logger.debug("	Compared MANIFEST.MF info:  " + unverified);
		System.out.printf("**********比對 MANIFEST.MF 資訊 **********\n");
		showMnftInfo(unverified);
		showMnftInfoWithConsole(unverified);
		logger.debug("	Original MANIFEST.MF info: " + verified);
		System.out.printf("**********未驗證 MANIFEST.MF 資訊**********\n");
		showMnftInfo(verified);
		showMnftInfoWithConsole(verified);
	}
	
	private static void showMnftInfo(APKManifest apkManifest) {
		logger.debug(String.format("	Size of entry map in MANIFEST.MF: %s", apkManifest.getMnftEntrySize()));
		logger.debug(String.format("	SHA1 for %s: %s", APVSConstantDescriptor.ANDROID_MANIFEST_XML, apkManifest.getShaForAndroidManifestXml()));
		logger.debug(String.format("	SHA1 for %s: %s", APVSConstantDescriptor.RESOURCES_ARSC, apkManifest.getShaForResourcesArsc()));
		logger.debug(String.format("	SHA1 for %s: %s", APVSConstantDescriptor.CLASSES_DEX, apkManifest.getShaForClassesDex()));
		logger.debug(String.format("	SHA1 for %s: %s", APVSConstantDescriptor.ANDROID_VERSION_CODE, apkManifest.getVersionCode()));
	}
	
	public static void showMnftInfoWithConsole(APKManifest apkManifest) {
		System.out.printf("未驗證 apk 檔案: %s" , apkManifest.getApkFileName());
		System.out.printf(" MANIFEST.MF 個數: %s\n", apkManifest.getMnftEntrySize());
		System.out.printf("	SHA1 for %s: %s\n", APVSConstantDescriptor.ANDROID_MANIFEST_XML, apkManifest.getShaForAndroidManifestXml());
		System.out.printf("	SHA1 for %s: %s\n", APVSConstantDescriptor.RESOURCES_ARSC, apkManifest.getShaForResourcesArsc());
		System.out.printf("	SHA1 for %s: %s\n", APVSConstantDescriptor.CLASSES_DEX, apkManifest.getShaForClassesDex());
		System.out.printf("	SHA1 for %s: %s\n", APVSConstantDescriptor.ANDROID_VERSION_CODE, apkManifest.getVersionCode());
	}
	
}
