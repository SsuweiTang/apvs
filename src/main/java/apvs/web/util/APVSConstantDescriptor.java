package apvs.web.util;

/**
 * @author Carl Adler (C.A.)
 * */
public class APVSConstantDescriptor {
	
	public static final String TARGET_KEY = "SHA1-Digest";
	public static final String ANDROID_MANIFEST_XML = "AndroidManifest.xml";
	public static final String RESOURCES_ARSC = "resources.arsc";
	public static final String CLASSES_DEX = "classes.dex";
	public static final String ANDROID_VERSION_CODE = "android:versionCode";
	
	public static final String FINGERPRINT_FOR_ENTIREAPKFILE="fingerPrintForEntireAPKFile";
	public static final String FINGERPRINT_ANDROID_MANIFEST_XML="AndroidManifest.xml";
	public static final String FINGERPRINT_RESOURCES_ARSC="resources.arsc";
	public static final String FINGERPRINT_CLASSES_DEX="classes.dex";
	public static final String FINGERPRINT_FOR_RES="RES";
	public static final String FINGERPRINT_FOR_RES_IMG="RES/IMG";
	public static final String FINGERPRINT_FOR_RES_XML="RES/XML";
	
	
	public static final int RESULT_CODE_FOR_PASSED = 1;
	public static final int RESULT_CODE_FOR_VERSION_MISMATCH = 2;
	public static final int RESULT_CODE_FOR_UNKNOWN = 3;
	public static final int RESULT_CODE_FOR_DANGEROUS = 4;
	
	public static final String LIGHT_PASSED = "green";
	public static final String LIGHT_DANGEROUS = "red";
	public static final String LIGHT_UNKNOWN = "yellow";
	public static final String LIGHT_VERSION_MISMATCH = "blue";
	
	public static final String MESSAGE_FOR_PASSED = "Successful.";
	public static final String MESSAGE_FOR_DANGEROUS = "Failed.";
	public static final String MESSAGE_FOR_UNKNOWN = "Unknown, no matched apk samples in database.";
	public static final String MESSAGE_FOR_VERSION_MISMATCH = "Version mismatch, please update the apk to the latest version.";
	
//	public static final String MESSAGE_FOR_DANGEROUS_ANDROID_MANIFEST_XML="AndroidManifest.xml mismatch.";
//	public static final String MESSAGE_FOR_DANGEROUS_CLASSES_DEX="program";
//	public static final String MESSAGE_FOR_DANGEROUS_RESOURCES_ARSCL="resources ";
//	public static final String MESSAGE_FOR_DANGEROUS_RES=" resource ";
//	public static final String MESSAGE_FOR_DANGEROUS_RES_IMG=" image ";
//	public static final String MESSAGE_FOR_DANGEROUS_RES_XML="XMl";
	
	public static final String MESSAGE_FOR_DANGEROUS_ANDROID_MANIFEST_XML="App權限/廣告";
	public static final String MESSAGE_FOR_DANGEROUS_CLASSES_DEX="程式碼";
	public static final String MESSAGE_FOR_DANGEROUS_RESOURCES_ARSCL="中文化資源檔";
	public static final String MESSAGE_FOR_DANGEROUS_RES="";
	public static final String MESSAGE_FOR_DANGEROUS_RES_IMG="圖片";
	public static final String MESSAGE_FOR_DANGEROUS_RES_XML="介面元件相關資源";

	
	
	public static final String RES_CATEGORY_PNG=".png";
	public static final String RES_CATEGORY_XML=".xml";
	public static final String RES_CATEGORY_JPG=".jpg";
	public static final String CERT_SF="META-INF/CERT.SF";
	public static final String CERT_RSA="META-INF/CERT.RSA";

	
	public static final String UPLOADED_FILE_LOCATION="D://uploaded/";
}
