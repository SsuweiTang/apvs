package apvs.web.util.fingerprint;

import apvs.web.dto.APKSubFileFingerPrint;


/**
 * 
 * @author Ssu-Wei,Tang
 *
 */
public class ObjectTransformer {
	
	public static APKSubFileFingerPrint transformSubCategoryOfResToListAPKSubFileFingerPrint(String ResCategory, String ResSubCategoryFingerPrint) {
		APKSubFileFingerPrint FingerResSubCategory = new APKSubFileFingerPrint();
		FingerResSubCategory.setFileName(ResCategory);
		FingerResSubCategory.setSha1Hex(ResSubCategoryFingerPrint);
		return FingerResSubCategory;
	}

}
